requirejs.config({
    "baseUrl": "js/lib",
    "paths": {
        jquery: "//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min",
        scripts: "../scripts"
    }
});

require([
    "jquery",
    "less.min",
    "bootstrap.min",
    //"imagesloaded.pkgd.min",
    "masonry.pkgd.min",
    "jquery.magnific-popup.min",
    "slides.min.jquery",
    "jquery-ui-1.8.23.custom.min",
    "jquery.mousewheel.min",
    "jquery.kinetic",
    "jquery.smoothdivscroll-1.3-min",
    "scripts"
], function(util) {
    //This function is called when scripts/helper/util.js is loaded.
    //If util.js calls define(), then this function is not fired until
    //util's dependencies have loaded, and the util argument will hold
    //the module value for "helper/util".
});