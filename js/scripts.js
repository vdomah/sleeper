//define(["jquery"], function($) {

$(window).scroll(function(){
    initHeader();
    initShopMenuFix();
});
$(document).ready(function(){
    $('body').prepend(
    '<div id="menu-verstka">' +
    link('mainpage') +
    link('blog') +
    link('blog_entry') +
    link('about') +
    link('account') +
    link('contacts') +
    link('publications') +
    link('shipping') +
    link('stocklists') +
    link('shop') +
    link('shop-item') +
    link('lookbook') +
    link('idea') +
    link('my_bag') +
    link('checkout-2') +
    link('checkout-3') +
    link('checkout-4') +
    link('login') +
    link('login-popup') +
    link('cartlist') +
    '</div>');

    initHeader();
    initZoomer(1);

    /*$('#container').imagesLoaded(function(){
        $('#container').masonry({
            itemSelector: '.item',
            gutter:58,
            isFitWidth:true
        });
    });*/

    var container = document.querySelector('#container');
    var msnry;
    // initialize Masonry after all images have loaded
    if (container)
    imagesLoaded( container, function() {
      msnry = new Masonry( container, {
          // options
          //columnWidth: 235,
          itemSelector: '.item',
          gutter:18,
          isFitWidth:true
        } );
    });

    $("#cart").click(function(){
        if($('#cart-list').hasClass("open")){
            $('#cart-list').removeClass("open");
        }else{
            $('#cart-list').addClass("open");
        }
    });
    $('.overlay-cart-list').on('click',function(){
        $('#cart-list').removeClass("open");
    });

    if ($('#products').length)
    $('#products').slides({
        preload: true,
        preloadImage: 'img/loading.gif',
        effect: 'slide, fade',
        crossfade: true,
        slideSpeed: 200,
        fadeSpeed: 500,
        generateNextPrev: true,
        generatePagination: false,
        animationComplete: function(current) {console.log(current)
            initZoomer(current);
        }
    });

        // lookbook page
    if ($('#makeMeScrollable').length)
        $("#makeMeScrollable").smoothDivScroll({
            hotSpotScrolling: false,
            touchScrolling: true,
            manualContinuousScrolling: true,
            mousewheelScrolling: false
        });

    if($('.open-popup-link').length)
    $('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.,
        open: function(){
            $('input').iCheck();
        }
    });

    if($('#test-popup').length)
    $.magnificPopup.open({
        items: {
            src: $('#test-popup').html(), // can be a HTML string, jQuery object, or CSS selector
            type: 'inline'
        },
        open: function(){
            $('input').iCheck();
        }
    });

        if($('.swiper-container').length)
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 30,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });

    //$("html").niceScroll();

    //if (typeof('iCheck') == "function") {
        $('input').iCheck();
    //}

    $('.mfp-container').click(function(){
    //    $.magnificPopup.close();
    });
  //  $('.sign_in_outer *').addClass('mfp-prevent-close');

    if($('.thumb img').length)
    $('.thumb img').adipoli({
        startEffect : 'grayscale',
        hoverEffect : 'normal'
    });

    $('.descriptions-links a').on('click',function(){
        var ch = $(this).parents('.exp-block');
        var d = ch.find('.description-text')
        if (ch.hasClass('open')) {
            ch.removeClass('open');
            //d.fadeOut();
        } else {
            ch.addClass('open');
            //d.fadeIn();
        }
    });

});//end document ready

//});

function link(link) {
    return '<a href="/'+link+'.html">'+link+'</a><br />'
}

function initHeader() {
    if ($(window).scrollTop() >= 300) {
        $('.header-transparent').addClass('border');
        $('.header').addClass('header-little');
    }
    else {
        $('.header-transparent').removeClass('border');
        $('.header').removeClass('header-little');
    }
}

function initShopMenuFix() {
    if ($(window).scrollTop() >= 400) {
        $('.menues').addClass('fixed');
    } else {
        $('.menues').removeClass('fixed');
    }
}

function detectBrowser() {

    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("safari/") !== -1) {
        // Looks like Safari on Windows (but browser detection is unreliable and best avoided)
    }
    if (ua.indexOf("windows") !== -1) {
        // Looks like Safari on Windows (but browser detection is unreliable and best avoided)
    }
    if (ua.indexOf("chrom")   === -1) {
        // Looks like Safari on Windows (but browser detection is unreliable and best avoided)
    }
}

function initZoomer(current) {
    if (!current)
        current = 1;

    //if ($('.slides_control > .photo-container').length)
    setTimeout(function(){
        $('.slides_control > .photo-container').eq(current-1).anythingZoomer({
            offsetX: 0,
            edge : 0
        });
        $('.slides_control > .photo-container').eq(current-1).bind('zoom unzoom', function(e, zoomer){
            if (e.type === "zoom") {
                $('.frame-wrap.active').show();
            } else {
                $('.frame-wrap.active').hide();
            }
        });
        $('.frame-wrap').removeClass('active');
        $('.frame-wrap').eq(current-1).addClass('active');
    },1);
}