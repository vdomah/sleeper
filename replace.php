<?php 
if(isset($_POST['find']))
{
    if(!in_array('on',$_POST))
        echo 'Не выбраны файлы!<br>';
    $find = htmlspecialchars ($_POST['find']);
    $replace = htmlspecialchars ($_POST['replace']);
    unset($_POST['find']);
    unset($_POST['replace']);
    
    foreach($_POST as $k=>$v)
    {
        $fname = str_replace('_','.',$k);
        $fcont = htmlspecialchars(file_get_contents($fname));
        $pos = strpos($fcont,$find);

        if($pos === false)
            echo 'В файле ' . $fname . ' не найдено совпадений.<br />';
        else
            echo $fname . ' - замена произведена.<br />';
        $fcont_new = str_replace($find,$replace,$fcont);
        file_put_contents($fname,htmlspecialchars_decode($fcont_new));
    }
}
?>
<form name="data" method="POST">
    <div style="float: left; width: 300px">
<?php
    $path = dirname(__FILE__).'/';
    $dh = opendir($path);
    while($file = readdir($dh))
    {
        if(is_file($file) && $file != basename(__FILE__))
            echo '<label><input type="checkbox" name="'.$file.'">'.$file.'</label><br>';
    }
?>
</div>
    <div>
        <label>Что искать:</label><br /><textarea name="find"><?php print($_POST['find'] ? $_POST['find'] : '')?></textarea><br />
        <label>Чем заменить:</label><br /><textarea name="replace"><?php print($_POST['replace'] ? $_POST['replace'] : '')?></textarea>
    </div>

    <input type="submit" value="Перехуяч" />
</form>